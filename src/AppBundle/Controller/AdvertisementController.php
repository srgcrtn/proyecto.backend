<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Advertisement;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class AdvertisementController extends Controller
{
    /**
     * @Route("/advertisements")
     * @Method({"GET"})
     */
    public function getAdvertisementsAction(Request $request)
    {
        $quantity = empty($request->query->get('l')) ? 10 : $request->query->get('l');
        $offset = empty($request->query->get('o')) ? 0 : $request->query->get('o');

        $advertisements = $this->getDoctrine()
            ->getRepository('AppBundle:Advertisement')
            ->findBy(array('isPublished' => '1'), array('relevance' => 'desc', 'createdAt' => 'desc'), $quantity, $offset);

        if (!$advertisements) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($advertisements, 'json', array('groups' => array('group1', 'group2')));

        return new JsonResponse([
                    'success' => true,
                    'data' => $data,
                ]);
    }

    /**
     * @Route("/advertisements/{id}")
     * @Method({"GET"})
     */
    public function getAdvertisementAction($id)
    {
        $advertisement = $this->getDoctrine()
            ->getRepository('AppBundle:Advertisement')
            ->findOneById($id);

        if (!$advertisement) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($advertisement, 'json', array('groups' => array('group2')));

        return new JsonResponse(['success' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/advertisements")
     * @Method({"POST"})
     */
    public function postAdvertisementAction(Request $request)
    {
        $advertisement = new Advertisement();

        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $advertisement->setTitle($request->request->get('title'));

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $advertisement->setUser($user);

        $validator = $this->get('validator');
        $errors = $validator->validate($advertisement);

        if (count($errors) > 0) {
            $errorArray = [];

            foreach ($errors as $error) {
                $errorArray[ $error->getPropertyPath() ] = $error->getMessage();
            }

            return new JsonResponse(['success' => false, 'errors' => $errorArray], 422);
        }

        try {
            $em = $this->getDoctrine()->getManager();

            $em->persist($advertisement);
            $em->flush();

            $serializer = $this->get('serializer');
            $data = $serializer->normalize($advertisement, 'json', array('groups' => array('group1', 'group2', 'group3')));

            return new JsonResponse(['success' => true, 'data' => $data]);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    /**
     * @Route("/advertisements/{id}")
     * @Method({"PATCH"})
     */
    public function updateAdvertisementAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $advertisement = $this->getDoctrine()
            ->getRepository('AppBundle:Advertisement')
            ->findOneById($id);

        if (!$advertisement) {
            return new JsonResponse(array('success' => false, 'message' => 'Advertisement not found'), 404);
        }

        // Recuperamos el usuario al que pertenece el token con el que se está identificando
        // Comprobamos si ambos usuarios coinciden (el de advertisement y el autenticado)
        $user = $advertisement->getUser();
        $authenticatedUser = $this->get('security.token_storage')->getToken()->getUser();
        if ($authenticatedUser !== $user) {
            return new JsonResponse(array('success' => false, 'message' => 'You are not allowed to edit this advertisement'), 404);
        }

        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);
        $updated = false;

        if ($request->request->get('title') !== null) {
            $advertisement->setTitle($request->request->get('title'));
            $updated = true;
        }

        if ($request->request->get('description') !== null) {
            $advertisement->setDescription($request->request->get('description'));
            $updated = true;
        }

        if ($request->request->get('isPublished') !== null) {
            $advertisement->setIsPublished($request->request->get('isPublished'));
            $updated = true;
        }

        if ($updated) {
            $validator = $this->get('validator');
            $errors = $validator->validate($advertisement);

            if (count($errors) > 0) {
                foreach ($errors as $error) {
                    $errorArray[ $error->getPropertyPath() ] = $error->getMessage();
                }

                return new JsonResponse(array('success' => false, 'errors' => $errorArray), 422);
            }

            try {
                $advertisement->setUpdatedAt(new \DateTime());
                $em->flush();

                $serializer = $this->get('serializer');
                $data = $serializer->normalize($advertisement, 'json', array('groups' => array('group1', 'group2', 'group3')));

                return new JsonResponse(array('success' => true, 'data' => $data));
            } catch (\Exception $e) {
                return new JsonResponse(array('success' => false, 'message' => $e->getMessage()), 422);
            }
        } else {
            return new JsonResponse(array('success' => true, 'message' => 'Nothing to update'), 200);
        }
    }

    /**
     * @Route("/advertisements/{id}")
     * @Method({"DELETE"})
     */
    public function deletedvertisementAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $advertisement = $this->getDoctrine()
            ->getRepository('AppBundle:Advertisement')
            ->findOneById($id);

        if (!$advertisement) {
            return new JsonResponse(array('success' => false, 'message' => 'Advertisement not found'), 404);
        }

        $user = $advertisement->getUser();
        $authenticatedUser = $this->get('security.token_storage')->getToken()->getUser();

        if ($authenticatedUser !== $user) {
            return new JsonResponse(array('success' => false, 'message' => 'You are not allowed to edit this advertisement'), 404);
        }

        $advertisement->setisActive(false);

        $validator = $this->get('validator');
        $errors = $validator->validate($advertisement);

        if (count($errors) > 0) {
            foreach ($errors as $error) {
                $errorArray[ $error->getPropertyPath() ] = $error->getMessage();
            }

            return new JsonResponse(array('success' => false, 'errors' => $errorArray), 422);
        }

        try {
            $em->flush();
            $serializer = $this->get('serializer');

            return new JsonResponse(array('success' => true, 'message' => 'Advertisement deleted'));
        } catch (\Exception $e) {
            return new JsonResponse(array('success' => false, 'message' => $e->getMessage()), 422);
        }
    }

    /**
     * @Route("/advertisements/{id}/tags")
     * @Method({"GET"})
     */
    public function getAdvertisementTagsAction($id)
    {
        $advertisement = $this->getDoctrine()
                ->getRepository('AppBundle:Advertisement')
                ->findOneById($id);

        if (!$advertisement) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        $tags = $advertisement->getTags();

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($tags, 'json');

        return new JsonResponse(['success' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/advertisements/{id}/tags/{idTag}")
     * @Method({"POST"})
     */
    public function postAdvertisementTagAction($id, $idTag)
    {
        $em = $this->getDoctrine()->getManager();

        $advertisement = $em->getRepository('AppBundle:Advertisement')->find($id);
        $tag = $em->getRepository('AppBundle:Tag')->find($idTag);

        if (!$advertisement || !$tag) {
            return new JsonResponse(['success' => true, 'data' => 'Advertisement '.$id.' or Tag '.$idTag.' not found'], 404);
        }

        try {
            $advertisement->addTag($tag);
            $em->flush();
            $serializer = $this->get('serializer');
            $data = $serializer->normalize($advertisement, 'json', array('groups' => array('group1', 'group2', 'group3')));
        } catch (\Exception $e) {
            return new JsonResponse(array('success' => false, 'errors' => $e->getMessage()), 422);
        }

        return new JsonResponse(['success' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/advertisements/{id}/tags/{idTag}")
     * @Method({"DELETE"})
     */
    public function deleteAdvertisementTagAction($id, $idTag)
    {
        $em = $this->getDoctrine()->getManager();

        $advertisement = $em->getRepository('AppBundle:Advertisement')->find($id);
        $tag = $em->getRepository('AppBundle:Tag')->find($idTag);

        if (!$advertisement || !$tag) {
            return new JsonResponse(['success' => true, 'data' => 'Advertisement '.$id.' or Tag '.$idTag.' not found'], 404);
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if($user !== $advertisement->getUser()) {
            return new JsonResponse(['success' => false, 'message' => 'You are not allowed to do this'], 404);
        }

        try {
            $advertisement->removeTag($tag);
            $em->flush();

            $serializer = $this->get('serializer');
            $data = $serializer->normalize($advertisement, 'json', array('groups' => array('group1', 'group2', 'group3')));
        } catch (\Exception $e) {
            return new JsonResponse(array('success' => false, 'errors' => $e->getMessage()), 422);
        }

        return new JsonResponse(['success' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/advertisements/{id}/comments")
     * @Method({"GET"})
     */
    public function getAdvertisementCommentsAction($id)
    {
        $advertisement = $this->getDoctrine()
                ->getRepository('AppBundle:Advertisement')
                ->findOneById($id);

        if (!$advertisement) {
            return new JsonResponse(['success' => false, 'message' => 'Not found'], 404);
        }

        $comments = $advertisement->getComments()->toArray();

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($comments, 'json', array('groups' => array('group2', 'group1')));

        return new JsonResponse(['success' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/advertisements/{id}/comments")
     * @Method({"POST"})
     */
    public function postAdvertisementCommentAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $advertisement = $em->getRepository('AppBundle:Advertisement')->find($id);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if (!$advertisement || !$user) {
            return new JsonResponse(['success' => true, 'data' => 'Advertisement or User not found'], 404);
        }

        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $comment = new Comment();
        $text = $request->request->get('text');
        $comment->setText($text);
        $comment->setAdvertisement($advertisement);
        $comment->setUser($user);

        $validator = $this->get('validator');
        $errors = $validator->validate($comment);

        if (count($errors) > 0) {
            $errorArray = [];

            foreach ($errors as $error) {
                $errorArray[ $error->getPropertyPath() ] = $error->getMessage();
            }

            return new JsonResponse(['success' => false, 'errors' => $errorArray], 422);
        }

        try {
            $em->persist($comment);
            $em->flush();
            $serializer = $this->get('serializer');
            $data = $serializer->normalize($comment, 'json', array('groups' => array('group2', 'group1')));
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => 'error', 'data' => $e->getMessage()), 422);
        }

        return new JsonResponse(['success' => true, 'data' => $data], 200);
    }

    /**
     * @Route("/advertisements/{id}/image")
     * @Method({"POST"})
     */
    public function postAdvertisementImageAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $advertisement = $em->getRepository('AppBundle:Advertisement')->find($id);
        $user = $this->get('security.token_storage')->getToken()->getUser();

        if (!$user || !$advertisement || ($advertisement->getUser() !== $user)) {
            return new JsonResponse(array('success' => false, 'message' => 'Usuario no encontrado o no tienes permisos para realizar la operación'), 404);
        }

        $file = $request->files->get('image');
        $image = new Image($file);
        $image->setName($file->getClientOriginalName());
        $image->setDocName();
        $image->setAdvertisement($advertisement);

        // Validamos
        $validator = $this->get('validator');
        $errors = $validator->validate($image);

        if (count($errors) > 0) {
            $errorArray = [];

            foreach ($errors as $error) {
                $errorArray[ $error->getPropertyPath() ] = $error->getMessage();
            }

            return new JsonResponse(array('success' => false, 'errors' => $errorArray), 422);
        }

        try {
            $image->uploadAdvertisementImage();
            $em->persist($image);
            $em->flush();

            return new JsonResponse(array('success' => true));
        } catch (\Exception $e) {
            return new JsonResponse(array('success' => false, 'message' => $e->getMessage()), 422);
        }
    }
}
