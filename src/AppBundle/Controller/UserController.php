<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Entity\Advertisement;
use AppBundle\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserController extends Controller
{
    /**
     * @Route("/users")
     * @Method({"GET"})
     */
    public function getUsersAction(Request $request)
    {
        $users = $this->getDoctrine()
            ->getRepository('AppBundle:User')
            ->findAll();

        if (!$users) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        // Parámetro por get == full? cambiamos el grupo de propiedades devueltas
        // Grupo2 incluye también los advertisements del usuario
        $chunk = $request->query->get('q') == 'full' ? 'group2' : 'group3';

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($users, 'json', array('groups' => array($chunk)));

        return new JsonResponse([
                    'success' => true,
                    'data' => $data,
                ]);
    }

    /**
     * @Route("/users/{username}")
     * @Method({"GET"})
     */
    public function getUserAction($username, Request $request)
    {
        $repository = $this->getDoctrine()->getRepository('AppBundle:User');

        $user = $repository->findOneBy(array('username' => $username));

        if (!$user) {
            return new JsonResponse(array('success' => false, 'message' => 'User not found'), 404);
        }

        // Parámetro por get == full? cambiamos el grupo de propiedades devueltas
        // Grupo2 incluye también los advertisements del usuario
        $chunk = $request->query->get('q') === 'full' ? 'group2' : 'group3';

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($user, 'json', array('groups' => array($chunk)));

        return new JsonResponse(array('success' => true, 'data' => $data), 200);
    }

    /**
     * @Route("/users")
     * @Method({"POST"})
     */
    public function postUserAction(Request $request)
    {
        // Transformamos la request para recibir bien los datos que envía angular
        // Angular envía los datos post como : Content-Type: application/json
        // Symfony los recibe como: application/x-www-form-urlencoded
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        // Creación de usuario y set de datos desde request
        $user = new User();
        $user->setName($request->request->get('name'));
        $user->setSurname($request->request->get('surname'));
        $user->setUsername($request->request->get('username'));
        $user->setEmail($request->request->get('email'));
        $user->setPassword($request->request->get('password'));

        // Validamos el objeto recién creado
        $validator = $this->get('validator');
        $errors = $validator->validate($user);

        // Si hay errores los almacenamos en un array asociativo propiedad error => mensaje
        // Y lo devolvemos
        if (count($errors) > 0) {
            $errorArray = [];

            foreach( $errors as $error )
            {
                $errorArray[ $error->getPropertyPath() ] =  $error->getMessage();
            }

            return new JsonResponse(array('success' => false, 'message' => $errorArray), 422);
        }

        // Password encoder
        $plainPassword = $request->request->get('password');
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $plainPassword);
        $user->setPassword($encoded);

        // Guardamos objeto
        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return new JsonResponse(array('success' => true, 'token' => $user->getToken()));
        } catch (\Exception $e) {
            return new JsonResponse(array('success' => false, 'message' => $e->getMessage()), 400);
        }
    }

    /**
     * @Route("/users/{username}")
     * @Method({"PUT"})
     */
    public function updateAction($username, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        // Buscamos usuario en la DB
        if (!$user) {
            return new JsonResponse(array('success' => false, 'message' => 'Usuario no encontrado'), 404);
        }

        // Recuperamos el usuario al que pertenece el token con el que se está identificando
        // Comprobamos si ambos usuarios coinciden
        $authenticatedUser = $this->get('security.token_storage')->getToken()->getUser();
        if($authenticatedUser !== $user) {
            return new JsonResponse(array('success' => false, 'message' => 'No tienes permisos para modificar este usuario'), 404);
        }

        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        if ($request->request->get('name') !== null) {
            $user->setName($request->request->get('name'));
        }
        if ($request->request->get('surname') !== null) {
            $user->setSurname($request->request->get('surname'));
        }
        if ($request->request->get('username') !== null) {
            $user->setUsername($request->request->get('username'));
        }
        if ($request->request->get('email') !== null) {
            $user->setEmail($request->request->get('email'));
        }
        if ($request->request->get('password') !== null) {
            $plainPassword = $request->request->get('password');
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $plainPassword);
            $user->setPassword($encoded);
        }

        $validator = $this->get('validator');
        $errors = $validator->validate($user);

        if (count($errors) > 0) {
            foreach( $errors as $error )
            {
                $errorArray[ $error->getPropertyPath() ] =  $error->getMessage();
            }

            return new JsonResponse(array('success' => false, 'errors' => $errorArray), 422);
        }

        try {
            $em->flush();

            return new JsonResponse(array('status' => 'success', 'data' => $user->serialize()));
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => 'error', 'data' => $e->getMessage()), 422);
        }
    }

    /**
     * @Route("/users/{username}")
     * @Method({"DELETE"})
     */
    public function deleteAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        if (!$user) {
            return new JsonResponse(array('status' => 'error', 'data' => 'Not found'), 404);
        }

        $authenticatedUser = $this->get('security.token_storage')->getToken()->getUser();
        if($authenticatedUser !== $user) {
            return new JsonResponse(array('success' => false, 'message' => 'No tienes permisos para modificar este usuario'), 404);
        }

        try {
            $user->setIsActive(false);
            $em->flush();

            return new JsonResponse(array('status' => 'success', 'data' => $user->serialize()));
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => 'error', 'data' => $e->getMessage()), 422);
        }
    }

    /**
     * @Route("/users/{username}/advertisements")
     * @Method({"GET"})
     */
    public function getUserAdvertisementsAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        if (!$user) {
            return new JsonResponse(array('status' => 'error', 'data' => 'Not found'), 404);
        }

        try {
            $advertisements = $user->getAdvertisements();

            $serializer = $this->get('serializer');
            $data = $serializer->normalize($advertisements, 'json', array('groups' => array('group2')));

            return new JsonResponse(array('status' => 'success', 'data' => $data));
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => 'error', 'data' => $e->getMessage()), 422);
        }
    }

    /**
     * @Route("/users/{username}/comments")
     * @Method({"GET"})
     */
    public function getUserCommentsAction($username)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        if (!$user) {
            return new JsonResponse(array('status' => 'error', 'data' => 'Not found'), 404);
        }

        try {
            $comments = $user->getComments();

            $serializer = $this->get('serializer');
            $data = $serializer->normalize($comments, 'json', array('groups' => array('group2')));

            return new JsonResponse(array('status' => 'success', 'data' => $data));
        } catch (\Exception $e) {
            return new JsonResponse(array('status' => 'error', 'data' => $e->getMessage()), 422);
        }
    }

    /**
     * @Route("/users/{username}/image")
     * @Method({"POST"})
     */
    public function postUserImageAction(Request $request, $username)
    {
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);
        $authenticatedUser = $this->get('security.token_storage')->getToken()->getUser();

        if (!$user || !$authenticatedUser || ($authenticatedUser !== $user) ) {
            return new JsonResponse(array('success' => false, 'message' => 'Usuario no encontrado o no tienes permisos para realizar la operación'), 404);
        }

        if(!$request->files->get('image')) {
            return new JsonResponse(array('success' => false, 'message' => "No image found"), 422);
        }

        $file = $request->files->get('image');

        // Comprobamos si ese usuario ya tiene imagen asociada  o creamos nueva imagen
        $image = $authenticatedUser->getImage() == null ? new Image() : $authenticatedUser->getImage();
        $image->setFile($file);
        $image->setName($file->getClientOriginalName());
        $image->setDocName();
        $image->setUser($authenticatedUser);

        // Validamos
        $validator = $this->get('validator');
        $errors = $validator->validate($image);

        if (count($errors) > 0) {
            $errorArray = [];

            foreach( $errors as $error )
            {
                $errorArray[ $error->getPropertyPath() ] =  $error->getMessage();
            }

            return new JsonResponse(array('success' => false, 'errors' => $errorArray), 422);
        }

        try {
            $image->uploadProfileImage();
            $em->persist($image);
            //$em->persist($authenticatedUser);
            $em->flush();

            return new JsonResponse(array('success' => true, 'data'=>$image->getPath()));
        } catch (\Exception $e) {
            return new JsonResponse(array('success' => false, 'message' => $e->getMessage()), 422);
        }
    }
}
