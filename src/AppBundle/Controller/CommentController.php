<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class CommentController extends Controller
{
    /**
     * @Route("/comments")
     * @Method({"GET"})
     */
    public function getCommentsAction()
    {
        $comments = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findAll();

        if (!$comments) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($comments, 'json', array('groups' => array('group3')));

        return new JsonResponse([
                    'success' => true,
                    'data' => $data,
                ]);
    }

    /**
     * @Route("/comments/{id}")
     * @Method({"DELETE"})
     */
    public function deleteCommentAction($id)
    {
        $comment = $this->getDoctrine()
            ->getRepository('AppBundle:Comment')
            ->findOneById($id);

        if (!$comment) {
            return new JsonResponse(['success' => false, 'message' => 'Not found'], 404);
        }

        $user = $this->get('security.token_storage')->getToken()->getUser();

        if ($user->getId() !== $comment->getUserId()) {
                return new JsonResponse(['success' => false, 'message' => 'Action not allowed'], 300);
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($comment);
            $em->flush();
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'message' => 'Generic error'], 502);
        }

        return new JsonResponse([
                    'success' => true,
                    'message' => 'Comment deleted',
                ]);
    }
}
