<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Util\SecureRandom;

class AuthController extends Controller
{
    /**
     * @Route("/login")
     * @Method({"POST"})
     */
    public function getLoginAction(Request $request)
    {
        // Transformamos la request para recibir bien los datos que envía angular
        // Angular envía los datos post como : Content-Type: application/json
        // Symfony los recibe como: application/x-www-form-urlencoded
        $data = json_decode($request->getContent(), true);
        $request->request->replace($data);

        $username = $request->request->get('username');
        $password = $request->request->get('password');

        if (!$username || !$password) {
            return new JsonResponse([
                        'success' => false,
                        'message' => 'Username and password required',
                    ], 422);
        }

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('AppBundle:User')->findOneByUsername($username);

        if (!$user) {
            return new JsonResponse([
                        'success' => false,
                        'message' => 'Invalid user',
                    ], 422);
        }

        if ($this->get('security.password_encoder')->isPasswordValid($user, $password)) {
            $serializer = $this->get('serializer');
            $data = $serializer->normalize($user, 'json', array('groups' => array('group2')));

            return new JsonResponse([
                        'apikey' => $user->getToken(),
                        'user' => $data,
                    ], 200);
        }

        return new JsonResponse([
                    'success' => false,
                    'message' => 'Invalid password',
                ], 422);
    }

    /**
     * @Route("/logout")
     * @Method({"GET"})
     */
    public function getLogoutAction()
    {
        $authenticatedUser = $this->get('security.token_storage')->getToken()->getUser();

        $generator = new SecureRandom();
        $random = $generator->nextBytes(30);
        $authenticatedUser->setToken(md5($random));

        try {
            $em = $this->getDoctrine()->getManager();

            $em->persist($authenticatedUser);
            $em->flush();

            return new JsonResponse([
                               'success' => true,
                               'data' => 'Usuario deslogueado',
                           ]);
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'message' => 'Database error'], 422);
        }
    }
}
