<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;

class TagController extends Controller
{
    /**
     * @Route("/tags")
     * @Method({"GET"})
     */
    public function getTagsAction()
    {
        $tags = $this->getDoctrine()
            ->getRepository('AppBundle:Tag')
            ->findAll();

        if (!$tags) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($tags, 'json', array('groups' => array('group1', 'group2')));

        return new JsonResponse([
                    'success' => true,
                    'data' => $data,
                ]);
    }

    /**
     * @Route("/tags/{id}/advertisements")
     * @Method({"GET"})
     */
    public function getTagsAdvertisementsAction($id)
    {
        $tag = $this->getDoctrine()
            ->getRepository('AppBundle:Tag')
            ->findOneById($id);

        if (!$tag) {
            return new JsonResponse(['success' => true, 'data' => 'Not found'], 404);
        }

        $advertisements = $tag->getAdvertisements();

        $serializer = $this->get('serializer');
        $data = $serializer->normalize($advertisements, 'json');

        return new JsonResponse([
                    'success' => true,
                    'data' => $data,
                ]);
    }

    /**
     * @Route("/tags")
     * @Method({"POST"})
     */
    public function postTagsAction(Request $request)
    {
        $tag = new Tag();
        $tag->setName($request->request->get('name'));

        $validator = $this->get('validator');
        $errors = $validator->validate($tag);

        if (count($errors) > 0) {
            $errorArray = [];

            foreach ($errors as $error) {
                $errorArray[ $error->getPropertyPath() ] = $error->getMessage();
            }

            return new JsonResponse(['success' => false, 'errors' => $errorArray], 422);
        }

        try {
            $em = $this->getDoctrine()->getManager();

            $em->persist($tag);
            $em->flush();

            $serializer = $this->get('serializer');
            $data = $serializer->normalize($tag, 'json');
        } catch (\Exception $e) {
            return new JsonResponse(['success' => false, 'message' => 'Database error'], 500);
        }

        return new JsonResponse(['success' => true, 'data' => $data]);
    }
}
