<?php
namespace AppBundle\Entity;


interface TagInterface {
    public function getId();
    public function getName();
    public function setName($name);
    public function getRelevance();
    public function setRelevance($relevance);
}
