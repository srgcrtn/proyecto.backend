<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Tag.
 *
 * @ORM\Entity
 * @ORM\Table()
 * @UniqueEntity("name")
 */
class Tag implements TagInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Advertisement", mappedBy="tags")
     **/
    private $advertisements;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=32)
     * @Assert\NotBlank()
     * @Assert\Length(min=3)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="relevance", type="integer")
     * @Assert\NotBlank()
     */
    private $relevance;

    /**
    * Constructor.
    * Creamos el ArrayCollection de anuncios con este tag
    * Inicializamos la relevancia
    */
    public function __construct()
    {
        $this->relevance = 0;
        $this->advertisements = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @Groups({"group1"})
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
    * Set advertisement
    * Añadimos el advertisement a nuestra colección
    */
    public function addAdvertisement(Advertisement $advertisement)
    {
        $this->advertisements[] = $advertisement;
    }

    /**
    * Get advertisements
    *
    * @Groups({"group1", "group2", "group3"})
    */
    public function getAdvertisements()
    {
        $advertisements = array();

        foreach ($this->advertisements->toArray() as $key => $value) {
            $advertisements[] = $value->getId();
        }

        return count($advertisements) > 0 ? $advertisements : null;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @Groups({"group2","group3"})
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set relevance.
     *
     * @param int $relevance
     *
     * @return Tag
     */
    public function setRelevance($relevance)
    {
        $this->relevance = $relevance;

        return $this;
    }

    /**
     * Get relevance.
     *
     * @Groups({"group2","group3"})
     *
     * @return int
     */
    public function getRelevance()
    {
        return $this->relevance;
    }
}
