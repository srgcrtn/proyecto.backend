<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Advertisement.
 *
 * @ORM\Table(name="advertisement")
 * @ORM\Entity
 */
class Advertisement
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="advertisements")
     * @ORM\JoinTable(name="advertisements_tags",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")})
     **/
    private $tags;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="advertisements")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="advertisement")
     */
    protected $comments;

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="advertisement")
     **/
    private $images;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(min=10)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Assert\Length(min=16)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $updatedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="relevance", type="integer")
     */
    private $relevance;

    /**
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(name="is_published", type="boolean")
     */
    private $isPublished;

    /**
     * Constructor. Inicializamos ArrayCollection de tags (Many To Many).
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->updatedAt = new \Datetime();
        $this->tags = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->relevance = 0;
        $this->isActive = true;
        $this->isPublished = false;
    }

    /**
     * Get id.
     *
     * @Groups({"group1"})
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add tag.
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag;
        $tag->addAdvertisement($this);

        return $this;
    }

    /**
     * Get tags.
     *
     * @Groups({"group2","group3"})
     *
     * @return array
     */
    public function getTags()
    {
        $tags = array();

        foreach ($this->tags->toArray() as $key => $value) {
            $tags[] = ['id' => $value->getId(), 'name' => $value->getName()];
        }

        return count($tags) > 0 ? $tags : null;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Advertisement
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @Groups({"group2"})
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return Advertisement
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @Groups({"group2"})
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return Advertisement
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @Groups({"group2"})
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt == null ? null : ['year' => $this->createdAt->format('Y'),
        'month' => $this->createdAt->format('m'), 'day' => $this->createdAt->format('d'), 'hour' => $this->createdAt->format('H'), 'minute' => $this->createdAt->format('m'), ];
    }

    /**
     * Set updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return Advertisement
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Advertisement
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get username.
     *
     * @Groups({"group2"})
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsername()
    {
        return $this->user->getUsername();
    }

    /**
     * Get User Profile Image.
     *
     * @Groups({"group2"})
     *
     * @return \AppBundle\Entity\User
     */
    public function getUserProfileImage()
    {
        return $this->user->getImage() == null ? null : $this->user->getImage()->getPath();
    }

    /**
     * Remove tags.
     *
     * @param \AppBundle\Entity\Tag $tags
     */
    public function removeTag(\AppBundle\Entity\Tag $tags)
    {
        $this->tags->removeElement($tags);
    }

    /**
     * Add comments.
     *
     * @param \AppBundle\Entity\Comment $comments
     *
     * @return Advertisement
     */
    public function addComment(\AppBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments.
     *
     * @param \AppBundle\Entity\Comment $comments
     */
    public function removeComment(\AppBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments.
     *
     * @return \Doctrine\Common\Collections\Collection
     * @Groups({"group2"})
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add images.
     *
     * @param \AppBundle\Entity\Image $images
     *
     * @return Advertisement
     */
    public function addImage(\AppBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images.
     *
     * @param \AppBundle\Entity\Image $images
     */
    public function removeImage(\AppBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images.
     *
     * @Groups({"group2", "group3"})
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Set relevance.
     *
     * @param int $relevance
     *
     * @return Advertisement
     */
    public function setRelevance($relevance)
    {
        $this->relevance = $relevance;

        return $this;
    }

    /**
     * Get relevance.
     *
     * @return int
     */
    public function getRelevance()
    {
        return $this->relevance;
    }

    /**
     * Set isActive.
     *
     * @param bool $isActive
     *
     * @return Advertisement
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isPublished.
     *
     * @param bool $isPublished
     *
     * @return Advertisement
     */
    public function setIsPublished($isPublished)
    {
        $this->isPublished = $isPublished;

        return $this;
    }

    /**
     * Get isPublished.
     *
     * @Groups({"group2", "group3"})
     *
     * @return bool
     */
    public function getIsPublished()
    {
        return $this->isPublished;
    }
}
