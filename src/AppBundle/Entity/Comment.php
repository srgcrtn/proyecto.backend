<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Comment
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="comments")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="Advertisement", inversedBy="comments")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id")
     * @Assert\NotBlank()
     */
    private $advertisement;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="text")
     * @Assert\Length(min=1)
     */
    private $text;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     * @Assert\NotBlank()
     */
    private $updatedAt;

    /**
     * Constructor. Inicializamos ArrayCollection de tags (Many To Many).
     */
    public function __construct()
    {
        $this->createdAt = new \Datetime();
        $this->updatedAt = new \Datetime();
    }

    /**
     * Get id
     *
     * @Groups({"group1","group3"})
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text
     *
     * @param string $text
     * @return Comment
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text
     *
     * @Groups({"group2","group3"})
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Comment
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @Groups({"group3"})
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set advertisement
     *
     * @param \AppBundle\Entity\Advertisement $advertisement
     * @return Comment
     */
    public function setAdvertisement(\AppBundle\Entity\Advertisement $advertisement = null)
    {
        $this->advertisement = $advertisement;

        return $this;
    }

    /**
     * Get advertisement
     *
     * @Groups({"group3"})
     * @return \AppBundle\Entity\Advertisement
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    /**
     * Get Advertisement ID
     *
     * @Groups({"group2", "group3"})
     * @return \AppBundle\Entity\Advertisement
     */
    public function getAdvertisementId()
    {
        return $this->advertisement->getId();
    }

    /**
     * Get Advertisement Title
     *
     * @Groups({"group2", "group3"})
     * @return \AppBundle\Entity\Advertisement
     */
    public function getAdvertisementTitle()
    {
        return $this->advertisement->getTitle();
    }

    /**
     * Get useranme
     *
     * @Groups({"group2", "group3"})
     * @return \AppBundle\Entity\Advertisement
     */
    public function getUsername()
    {
        return $this->user->getUsername();
    }

    /**
     * Get createdAt.
     *
     * @Groups({"group2"})
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt == null ? null : ['year' => $this->createdAt->format('Y'),
        'month' => $this->createdAt->format('m'), 'day' => $this->createdAt->format('d'), 'hour' => $this->createdAt->format('H'), 'minute' => $this->createdAt->format('m'), ];
    }
}
