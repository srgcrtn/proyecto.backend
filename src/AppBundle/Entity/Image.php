<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Image.
 *
 * @ORM\Entity
 * @ORM\Table()
 */
class Image
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $id;

    /**
     * @ORM\OneToOne(targetEntity="User", inversedBy="image")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     **/
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Advertisement", inversedBy="images")
     * @ORM\JoinColumn(name="advertisement_id", referencedColumnName="id")
     **/
    private $advertisement;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank
     */
    public $docName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    public $path;

    /**
     * @Assert\Image(
     *     minWidth = 400,
     *     maxWidth = 1200,
     *     minHeight = 300,
     *     maxHeight = 900
     * )
     */
    protected $file;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Doc Name.
     *
     *
     * @return Image
     */
    public function setDocName()
    {
        $this->docName = md5(uniqid()).'.'.$this->getFile()->guessExtension();

        return $this;
    }

    /**
     * Set Doc Name.
     *
     *
     * @return Image
     */
    public function getDocName()
    {
        return $this->docName;
    }

    /**
     * Set Doc Name.
     *
     *
     * @return Image
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @Groups({"group2", "group3"})
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set path.
     *
     * @param string $path
     *
     * @return Image
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path.
     *
     * @Groups({"group2", "group3"})
     * @return string
     */
    public function getPath()
    {
        return $this->path === null ? null : $this->path;
    }

    /**
     * Set user.
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Image
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;
        $user->setImage($this);

        return $this;
    }

    /**
     * Get user.
     *
     * @return \AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set advertisement.
     *
     * @param \AppBundle\Entity\Advertisement $advertisement
     *
     * @return Image
     */
    public function setAdvertisement(\AppBundle\Entity\Advertisement $advertisement = null)
    {
        $this->advertisement = $advertisement;
        $advertisement->addImage($this);

        return $this;
    }

    /**
     * Get advertisement.
     *
     * @return \AppBundle\Entity\Advertisement
     */
    public function getAdvertisement()
    {
        return $this->advertisement;
    }

    /**
     * Sets file.
     *
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * Get file.
     *
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getAbsolutePath()
    {
        return null === $this->path ? null : $this->getUploadRootDir().'/'.$this->path;
    }

    public function getWebPath()
    {
        return null === $this->path ? null : $this->getUploadDir().'/'.$this->path;
    }

    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'uploads/images';
    }

    public function uploadProfileImage()
    {
        $usersDir = '/users/';

        $this->getFile()->move(
            $this->getUploadRootDir().$usersDir,
            $this->docName
        );

        $this->path = $this->getUploadDir().$usersDir.$this->getDocName();

        $this->file = null;
    }

    public function uploadAdvertisementImage()
    {
        $advertisementsDir = '/advertisements/'.$this->advertisement->getId().'/';

        $this->getFile()->move(
                $this->getUploadRootDir().$advertisementsDir,
                $this->docName
            );

        $this->path = $this->getUploadDir().$advertisementsDir.$this->getDocName();

        $this->file = null;
    }
}
