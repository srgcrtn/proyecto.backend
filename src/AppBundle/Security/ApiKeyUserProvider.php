<?php

namespace AppBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Doctrine\ORM\EntityManager;

class ApiKeyUserProvider implements UserProviderInterface
{
    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getUsernameForApiKey($apiKey)
    {
        $user = $this->em->getRepository('AppBundle:User')
                ->findOneBy(array('token' => $apiKey));

        return $user ? $user->getUsername() : null;
    }

    public function loadUserByUsername($username)
    {
        $user = $this->em->getRepository('AppBundle:User')->createQueryBuilder('u')
           ->where('u.username = :username OR u.email = :email')
           ->setParameter('username', $username)
           ->setParameter('email', $username)
           ->getQuery()
           ->getOneOrNullResult();

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}
